package com.licenta.ann.result;

public class TestResult {

	private int nrCorectPredictions;
	private int length;

	public TestResult(int nrCorectPredictions, int length) {
		this.setNrCorectPredictions(nrCorectPredictions);
		this.setLength(length);
	}

	public TestResult() {
	}

	@Override
	public String toString() {
		StringBuilder ret = new StringBuilder();
		ret.append("ON TEST DATA \n");
		ret.append("Acuratete:" + getAcuratete() + "\n");
		ret.append("nrCorectPredictions:" + getNrCorectPredictions() + " nrDateDeTest:" + getLength() + "\n");
	
		return ret.toString();
	}

	public int getNrCorectPredictions() {
		return nrCorectPredictions;
	}

	public void setNrCorectPredictions(int nrCorectPredictions) {
		this.nrCorectPredictions = nrCorectPredictions;
	}

	public int getLength() {
		return length;
	}

	public void setLength(int length) {
		this.length = length;
	}

	public double getAcuratete() {
		return (((double) getNrCorectPredictions() / (double) getLength()) * 100d);
	}	
	

}
