package com.licenta.ann;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import com.licenta.ann.activare.ActivationFunction;

/**
 * Se ocupa cu antrenarea unei retele simple
 * 
 * @author User
 * 
 */
public class NetworkTrainer {
	private NeuronalNetwork netw;

	private float learningRate;

	/** mementum pentru bias din Input - Hidden * */
	private float[] momentumBiasInHi;

	/** mementum pentru bias din Hidden - Output * */
	private float[] momentumBiasHiOut;

	public NetworkTrainer(NeuronalNetwork netw) {
		this(netw, 0.3f);
	}

	public NetworkTrainer(NeuronalNetwork netw, float learningRate) {
		this.netw = netw;
		this.learningRate = learningRate;
		initMomentBias();
	}

	/**
	 * Reinitializeaza toate valoarile pentru momentum bias cu 0
	 */
	public void initMomentBias() {
		momentumBiasInHi = new float[netw.getNumberOfHiddenNeurons()];
		momentumBiasHiOut = new float[netw.getNumberOfOutputNeurons()];
	}

	public float train(float[] input, float[] expectedOut, float weightDecay) {
		PropagationResult result = netw.forwardPropagation(input);
		backPropagation(input, result, expectedOut, weightDecay);
		return computeError(result.getOutputs(), expectedOut);
	}

	private float computeError(float[] networkOut, float[] expectedOut) {
		float error = 0;
		for (int i = 0; i < netw.getNumberOfOutputNeurons(); i++) {
			error += (networkOut[i] - expectedOut[i])
					* (networkOut[i] - expectedOut[i]);
		}
		return error;
	}

	/**
	 * Face backPropagation pentru un exemplu
	 * 
	 * @param input
	 * @param result
	 * @param expectedOut
	 */
	private void backPropagation(float[] input, PropagationResult result,
			float[] expectedOut, float weightDecay) {
		float[] networkOut = result.getOutputs();

		ActivationFunction activationF = netw.getActivationF();
		// calculam eroarea pe stratul de iesire
		float[] outputDelta = new float[networkOut.length];
		for (int i = 0; i < networkOut.length; i++) {
			outputDelta[i] = (expectedOut[i] - networkOut[i])
					* activationF.fDeriv(networkOut[i]);
		}
		// calculam eroarea pe stratul ascuns
		float[] networkHiddenOut = result.getHiddenResults();

		float[] hiddenDelta = new float[networkHiddenOut.length];
		for (int i = 0; i < netw.getNumberOfHiddenNeurons(); i++) {
			float sum = 0;
			for (int j = 0; j < networkOut.length; j++) {
				sum += netw.getWeigthHiddenOut(i, j) * outputDelta[j];
			}
			hiddenDelta[i] = activationF.fDeriv(networkHiddenOut[i]) * sum;
		}

		// actualizam ponderile hidenOutput
		for (int i = 0; i < networkOut.length; i++) {
			for (int j = 0; j < netw.getNumberOfHiddenNeurons(); j++) {
				float val = weightDecay*learningRate * outputDelta[i]
						* networkHiddenOut[j];
				float aux = netw.getWeigthHiddenOut(j, i) + val;
				netw.setWeigthHiddenOut(j, i, aux);
			}
			// actualizez si pentru bias
			momentumBiasHiOut[i] = learningRate * outputDelta[i];
			float aux = netw.getBiasInHidden(i) + weightDecay*momentumBiasHiOut[i];
			netw.setBiasInHidden(i, aux);
		}

		// actualizam ponderile inputHidden
		for (int i = 0; i < netw.getNumberOfHiddenNeurons(); i++) {
			for (int j = 0; j < input.length; j++) {
				float val = weightDecay*learningRate * hiddenDelta[i] * input[j];
				float aux = netw.getWeigthInHidden(j, i) + val;
				netw.setWeigthInHidden(j, i, aux);
			}
			// actualizez si pentru bias
			momentumBiasInHi[i] = learningRate * hiddenDelta[i];
			float aux = netw.getBiasInInput(i) + weightDecay*momentumBiasInHi[i];
			netw.setBiasInInput(i, aux);
		}
	}

	/**
	 * Antreneaza reteaua folosind un set de date
	 * 
	 * @param in
	 *            datele de intrare
	 * @param expectedOut
	 *            rezultate asteptate
	 * @param nrMaxIterations
	 *            numarul maxim de ture de antrenament
	 * @param weightDecay 
	 * @return true daca s-a obtinut errTrashold dorit, false daca s-a oprit
	 *         antrenarea la nrMaxIterations
	 */
	public boolean train(float[][] in, float[][] expectedOut,
			long nrMaxIterations, float errThreshold, float weightDecay) {
		float err = 0;
		for (long i = 0; i < nrMaxIterations; i++) {
			err = trainInOrder(in, expectedOut, err, weightDecay);
			// err = trainInRandomOrder(in, expectedOut, err);
			err = err / in.length;
			// System.out.println("eroare la iteratia: "+i+" este:"+err);
			if (err < errThreshold) {
				System.out.println("eroare la iteratia: " + i + " este:" + err);
				// am optinut eroarea dorita
				return true;
			}
		}
		System.out.println("eroare dupoa toate iteratiile este:" + err);
		return false;
	}

	/**
	 * Alege datele in ordine aleatoare
	 * 
	 * @param in
	 * @param expectedOut
	 * @param err
	 * @return erroarea
	 */
	private float trainInRandomOrder(float[][] in, float[][] expectedOut,
			float err,float weightDecay) {
		List<Integer> pozitions = new ArrayList<Integer>(in.length);
		for (int j = 0; j < in.length; j++) {
			pozitions.add(j);
		}
		Random rnd = new Random();
		while (pozitions.size() > 0) {
			// aleg aleator un test
			int random = rnd.nextInt(pozitions.size());
			int poz = pozitions.remove(random);
			// calculez eroarea
			err += train(in[poz], expectedOut[poz], weightDecay);
			// System.out.println("eroarea este:"+err);
		}
		return err;
	}

	/**
	 * Alege exercitiile in ordine
	 * 
	 * @param in
	 * @param expectedOut
	 * @param err
	 * @param weightDecay 
	 * @return
	 */
	private float trainInOrder(float[][] in, float[][] expectedOut, float err, float weightDecay) {
		for (int j = 0; j < in.length; j++) {
			err += train(in[j], expectedOut[j], weightDecay);
		}
		return err;
	}

}
