package com.licenta.ann;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.StringTokenizer;

import com.licenta.model.HeartDisease;

public class DataSetUtil {
	private float[][] inputDataLearn;
	private float[][] inputDataTest;
	private float[][] dataSet;
	
	
	public DataSetUtil(){
		try {
			float[][] inputData = getInputData();
		
			int nrDateDeAntrenare = (inputData.length * 2) / 3;
			int nrDateDeTest = inputData.length - nrDateDeAntrenare;
			
			inputDataLearn = new float[nrDateDeAntrenare][];
			for (int i = 0; i < nrDateDeAntrenare; i++) {
				inputDataLearn[i] = new float[inputData[i].length];
				for (int j = 0; j < inputData[i].length; j++) {
					inputDataLearn[i][j] = inputData[i][j];
				}
			}
			
			
			inputDataTest = new float[nrDateDeTest][];
			for (int i = nrDateDeAntrenare; i < inputData.length; i++) {
				inputDataTest[i-nrDateDeAntrenare] = new float[inputData[i].length];
				for (int j = 0; j < inputData[i].length; j++) {
					inputDataTest[i-nrDateDeAntrenare][j] = inputData[i][j];
				}
			}
			
			
		} catch (NumberFormatException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	public float[][] getDataSet(){
		return dataSet;
	}
	public float[][] getInputData() throws NumberFormatException, IOException {
		File f = new File("res/processed.cleveland.data");
		FileReader fr = new FileReader(f);
		BufferedReader bufR = new BufferedReader(fr);
		int nrInreg = Integer.parseInt(bufR.readLine());
		int nrAtr = Integer.parseInt(bufR.readLine());
		dataSet = new float[nrInreg][];

		float[] max = new float[]{HeartDisease.ageMax,HeartDisease.genderMax, HeartDisease.chestPainTypeMax,
							HeartDisease.restingBloodPressureMax, HeartDisease.cholesterolMax, HeartDisease.fastingBloodSugarMax,
							HeartDisease.restingECGMax, HeartDisease.maximumHeartRateMax, HeartDisease.exerciseInducedAnginaMax,
							HeartDisease.oldPeakMax, HeartDisease.slopMax, HeartDisease.numberOfVesselsColoredMax, HeartDisease.thalMax};
		float[] min = new float[]{HeartDisease.ageMin,HeartDisease.genderMin, HeartDisease.chestPainTypeMin,
				HeartDisease.restingBloodPressureMin, HeartDisease.cholesterolMin, HeartDisease.fastingBloodSugarMin,
				HeartDisease.restingECGMin, HeartDisease.maximumHeartRateMin, HeartDisease.exerciseInducedAnginaMin,
				HeartDisease.oldPeakMin, HeartDisease.slopMin, HeartDisease.numberOfVesselsColoredMin, HeartDisease.thalMin };
		
		float[] frequent = new float[]{HeartDisease.ageFrequent,HeartDisease.genderFrequent, HeartDisease.chestPainTypeFrequent,
				HeartDisease.restingBloodPressureFrequent, HeartDisease.cholesterolFrequent, HeartDisease.fastingBloodSugarFrequent,
				HeartDisease.restingECGFrequent, HeartDisease.maximumHeartRateFrequent, HeartDisease.exerciseInducedAnginaFrequent,
				HeartDisease.oldPeakFrequent, HeartDisease.slopFrequent, HeartDisease.numberOfVesselsColoredFrequent, HeartDisease.thalFrequent };
		
		for (int i = 0; i < nrInreg; i++) {
			dataSet[i] = new float[nrAtr + 1];
			String line = bufR.readLine();
			StringTokenizer st = new StringTokenizer(line, " ");		
			for (int j = 0; j <= nrAtr; j++) {
				String atr = st.nextToken();
				if(atr.equals("?")){
					dataSet[i][j] = frequent[j];
				}else{
					dataSet[i][j] = Float.parseFloat(atr);
				}
				
			}

		}
		bufR.close();

		// normalizam datele
		for (int j = 0; j < nrAtr; j++) {
			float aux = max[j] - min[j];
			for (int i = 0; i < nrInreg; i++) {
				dataSet[i][j] = (dataSet[i][j] - min[j]) / aux;	
			}
			
		}
		// reduce problema la una binara
		for(int i=0;i<nrInreg; i++){
			if( dataSet[i][nrAtr] > 0f ){
				dataSet[i][nrAtr] = 1f;
			}
			
		}
					
		return dataSet;
	}

	public float[][] getLearnData() {
		return inputDataLearn;
	}
	public float[][] getTestData() {
		return inputDataTest;
	}

}
