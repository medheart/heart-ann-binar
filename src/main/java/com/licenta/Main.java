package com.licenta;

import com.licenta.Consola;

public class Main {

	public static void main(String[] args) {
		HeartController controller = new HeartController();
		Consola consola = new Consola(controller);
		consola.start();
	}
}
