package com.licenta;

import java.util.Scanner;

import com.licenta.ann.result.TestResult;
import com.licenta.model.HeartDisease;


public class Consola {

	private HeartController controller;

	public Consola(HeartController controller) {
		this.controller = controller;
	}

	public void start() {
		boolean ok = true;
		Scanner scanner = new Scanner(System.in);
		while (ok ) {
			printMenu();	
			String cmd;
			try {
				System.out.println("Dati cmd = ");
				cmd = scanner.next();
				switch (cmd) {
				case "1":
					learn();
					break;
				case "2":
					testData();	
					break;
				case "3":
					crossValidation();
					break;
				case "0":
					ok = false;
					break;
				default:
					System.out.println("Invalid!");
					break;
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		scanner.close();
	}

	private void crossValidation() {
		controller.doCrossValidation10Fold();
		
	}

	private void predictie() {
		System.out.println("Dati datele ");
		
		HeartDisease disease = new HeartDisease();
	
		
		System.out.println("end");
	}

	private void testData() {
		System.out.println("Start test");
		
		TestResult result = controller.doTest();
		System.out.println(result);
		
		System.out.println("End test");
		
	}

	private void learn() {
		while(true){
			System.out.println("Start learn");
			
			TestResult result = controller.doLearn();
			
			System.out.println(result);
			
			System.out.println("End learn");
		}
		
	}

	private void printMenu() {
		System.out.println("---------------- Menu Heart------------");
		System.out.println("1. Invatare pe data si salveaza versiunea mai buna ");
		System.out.println("2. Testare pe data de test");
		System.out.println("3. Cross validation 10-fold");
		// System.out.println("3. Predictie");
		System.out.println("0. Exit");

		System.out.println("----------------  End Menu Heart ------------");

	}
}
