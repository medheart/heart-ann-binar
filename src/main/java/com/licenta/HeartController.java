package com.licenta;


import java.util.ArrayList;
import java.util.List;

//import java.io.FileReader;
//import java.io.FileWriter;
//import java.io.IOException;
//import java.io.Reader;
//
//import com.google.gson.Gson;
import com.licenta.ann.DataSetUtil;
import com.licenta.ann.NetworkCreator;
import com.licenta.ann.NetworkTrainer;
import com.licenta.ann.NeuronalNetwork;
import com.licenta.ann.result.LearnResult;
import com.licenta.ann.result.TestResult;

public class HeartController {

	private NeuronalNetwork netw;
	private DataSetUtil dataSetUtil = new DataSetUtil();
//	private NeuronalNetwork bestANN;
//	private String filename;

	public TestResult doTest() {
		return doTest();
	}

	public TestResult doLearn() {
		float learningRate = 0.3f;
		long nrMaxIterations = 999l;
		float errThreshold = 0.0000001f;

		TestResult result = new TestResult();

		result = doLearn(learningRate, nrMaxIterations, errThreshold);

		return result;
	}

	private TestResult doLearn(float learningRate, long nrMaxIterations, float errThreshold) {

		float[][] inputDataLearn = dataSetUtil.getLearnData();

		int nrIntrari = inputDataLearn[0].length - 1;
		int nrIesiri = 2;
		int nrHidden = (int) Math.sqrt(nrIesiri * nrIntrari);

		// nrHidden=(int)(1+(2.0*nrIntrari)/3);

		System.out.println("nrInput:" + nrIntrari);
		System.out.println("nrHidden:" + nrHidden);
		System.out.println("nrOutput:" + nrIesiri);

		// creez un NeuronalNetwork
		netw = NetworkCreator.createRandom(nrIntrari, nrHidden, nrIesiri, 0.5f);

		// creez antrenorul de retea
		NetworkTrainer trainer = new NetworkTrainer(netw, learningRate);

		// construiesc datele de antrenare
		float[][] in = new float[inputDataLearn.length][];
		float[][] expectedOut = new float[inputDataLearn.length][];
		for (int i = 0; i < inputDataLearn.length; i++) {
			in[i] = new float[inputDataLearn[i].length - 1];
			for (int j = 0; j < inputDataLearn[i].length - 1; j++) {
				in[i][j] = inputDataLearn[i][j];
			}
			expectedOut[i] = new float[6];
			expectedOut[i][(int) inputDataLearn[i][inputDataLearn[i].length - 1]] = 1f;
		}

		// fac antrenarea
		float weightDecay = 0.0001f;
		trainer.train(in, expectedOut, nrMaxIterations, errThreshold, weightDecay);

		// fac predictie.. folosesc tot datele de antrenare
		int nrCorectPredictions = 0;
		for (int i = 0; i < inputDataLearn.length; i++) {
			float[] inValues = in[i];
			float[] predict = netw.predict(inValues);

			if (expectedOut[i][(int) predict[0]] == 1f) {
				nrCorectPredictions++;
			}
		}
		netw.bestCorectPrediction = nrCorectPredictions;

		// pe datele de test
		TestResult result = doTest(netw);
		netw.bestCorectPrediction += result.getNrCorectPredictions();

		//saveOnFile(nrCorectPredictions);
		LearnResult learnResult = new LearnResult(nrCorectPredictions, inputDataLearn.length);
		System.out.println(learnResult);
		return result;

	}

	private TestResult doTest(NeuronalNetwork neuronalNetwork) {
		// fac predictie.. folosesc tot datele care nu au fost folosite la
		// antrenare
		float[][] inputDataTest = dataSetUtil.getTestData();
		int nrCorectPredictions = 0;

		for (int i = 0; i < inputDataTest.length; i++) {
			float[] inValues = new float[inputDataTest[i].length - 1];
			for (int j = 0; j < inputDataTest[i].length - 1; j++) {
				inValues[j] = inputDataTest[i][j];
			}
			float[] predict = neuronalNetwork.predict(inValues);

			// System.out.println(predict[0] + " " + predict[1]);
			if (inputDataTest[i][inputDataTest[i].length - 1] == predict[0]) {
				nrCorectPredictions++;
			}
		}
		return new TestResult(nrCorectPredictions, inputDataTest.length);
	}

	public void doCrossValidation10Fold(){
		
		List<TestResult> testResults = new ArrayList<>();
		
		float[][] dataSet = dataSetUtil.getDataSet(); // toata datele
		int numberBucket = 10;
		List<float[][]> buckets = extractedBucket(dataSet, numberBucket);
		
		// pregatire ann
		long nrMaxIterations = 9000;
		float learningRate = 0.3f;
		float errThreshold = 0.0000001f;
		float weightDecay = 0.001f;
		
		int nrIntrari = dataSet[0].length - 1;
		int nrIesiri = 2;
		int nrHidden = (int) Math.sqrt(nrIesiri * nrIntrari);
		
		
		for(int numberCross = 0; numberCross< 10; numberCross++){
			
			float[][] inputDataLearn = combineBucketExcepted(buckets, numberCross, numberBucket);
			
			System.out.println("nrInput:" + nrIntrari);
			System.out.println("nrHidden:" + nrHidden);
			System.out.println("nrOutput:" + nrIesiri);
	
			// creez un NeuronalNetwork
			netw = NetworkCreator.createRandom(nrIntrari, nrHidden, nrIesiri, 0.5f);
	
			// creez antrenorul de retea
			NetworkTrainer trainer = new NetworkTrainer(netw, learningRate);
	
			// construiesc datele de antrenare
			float[][] in = new float[inputDataLearn.length][];
			float[][] expectedOut = new float[inputDataLearn.length][];
			for (int i = 0; i < inputDataLearn.length; i++) {
				in[i] = new float[inputDataLearn[i].length - 1];
				for (int j = 0; j < inputDataLearn[i].length - 1; j++) {
					in[i][j] = inputDataLearn[i][j];
				}
				expectedOut[i] = new float[6];
				expectedOut[i][(int) inputDataLearn[i][inputDataLearn[i].length - 1]] = 1f;
			}
	
			// fac antrenarea
			
			trainer.train(in, expectedOut, nrMaxIterations, errThreshold, weightDecay);
	
			System.out.println("------- Test ----");
			// fac predictie pentru date de antrenare
			int nrCorectPredictions = 0;
			for (int i = 0; i < inputDataLearn.length; i++) {
				float[] inValues = in[i];
				float[] predict = netw.predict(inValues);
	
				if (expectedOut[i][(int) predict[0]] == 1f) {
					nrCorectPredictions++;
				}
			}
			
			LearnResult learnResult = new LearnResult(nrCorectPredictions, inputDataLearn.length);
			System.out.println(learnResult);
			
			// fac predictie pe data de test
			float[][] inputDataTest = buckets.get(numberCross);
			nrCorectPredictions = 0;
	
			for (int i = 0; i < inputDataTest.length; i++) {
				float[] inValues = new float[inputDataTest[i].length - 1];
				for (int j = 0; j < inputDataTest[i].length - 1; j++) {
					inValues[j] = inputDataTest[i][j];
				}
				float[] predict = netw.predict(inValues);
	
				// System.out.println(predict[0] + " " + predict[1]);
				if (inputDataTest[i][inputDataTest[i].length - 1] == predict[0]) {
					nrCorectPredictions++;
				}
			}
			TestResult testR = new TestResult(nrCorectPredictions, inputDataTest.length);
			testResults.add(testR);
			System.out.println(testR);
			
		}
		
		// do average
		System.out.println("------ Average test --------");
		
		Double sum = testResults.stream().mapToDouble((test)-> test.getAcuratete()).sum();
		System.out.println("Acuratetea este : "+ (double)sum/(double)testResults.size());
		
	
	}
	private float[][] combineBucketExcepted(List<float[][]> buckets, int exceptedBucket, int numberBucket) {
		float[][] rez;
		if(exceptedBucket == 9){ // last bucket excluded
			rez = new float[buckets.get(0).length*(numberBucket-1)+1][buckets.get(0)[0].length];
		} else {
			rez = new float[buckets.get(0).length*(numberBucket-1)+4][buckets.get(0)[0].length];
		}
		
		int index = 0;
		for(int i=0;i<numberBucket;i++){
			if(i==exceptedBucket) continue;
			for(int j=0;j<buckets.get(i).length;j++){
				rez[index++] = buckets.get(i)[j];
			}
		}
		return rez;
	}

//	private void loadBestAnnJson() {
//		Gson gson = new Gson();
//        try (Reader reader = new FileReader(filename)) {
//			// Convert JSON to Java Object
//            bestANN = gson.fromJson(reader, NeuronalNetwork.class);
//        } catch (IOException e) {
//            e.printStackTrace();
//        }        
//	}
//	
//	protected void saveOnFile(int nrCorectPredictions) {
//		checkBestANN(nrCorectPredictions);
//		Gson gson = new Gson();
//		
//		try (FileWriter writer = new FileWriter(filename)) {
//		
//		    gson.toJson(bestANN, writer);
//		
//		} catch (IOException e) {
//		    e.printStackTrace();
//		}
//
//	}
//	private void checkBestANN(int nrCorectPredictions) {
//		int bestCorectPrediction = 0;
//		if(bestANN != null){
//			bestCorectPrediction = bestANN.bestCorectPrediction;
//		}
//		
//		TestResult testResult = doTest(netw);
//		if( bestANN == null){
//			bestANN = netw;
//			bestCorectPrediction = nrCorectPredictions;
//			
//			bestCorectPrediction += testResult.getNrCorectPredictions();
//			
//			bestANN.bestCorectPrediction = bestCorectPrediction;
//		}
//		if(bestCorectPrediction <  nrCorectPredictions + testResult.getNrCorectPredictions()){
//			bestANN = netw;
//			bestCorectPrediction = nrCorectPredictions;
//			
//			bestCorectPrediction += testResult.getNrCorectPredictions();
//			bestANN.bestCorectPrediction = bestCorectPrediction;
//		}
//	}
//	

	private List<float[][]> extractedBucket(float[][] dataSet, int numberBucket) {
		List<float[][]> buckets = new ArrayList<>();
		
		int sizeBucket = dataSet.length/numberBucket;
		
		for(int i=0;i<numberBucket-1 ;i++){
			int start = i*sizeBucket;
			int end = (i+1)*sizeBucket;
			
			float[][] bucket = new float[sizeBucket][dataSet[0].length];
			int index = 0;
			for(int k=start;k<end;k++){
				bucket[index++] = dataSet[k];
			}
			buckets.add(bucket);
		}
		// add last pacient in last bucket
		float[][] bucket = new float[sizeBucket+3][dataSet[0].length];
		int index = 0;
		for(int k=270; k< dataSet.length;k++){
			bucket[index++] = dataSet[k];
		}
		buckets.add(bucket);
		
		return buckets;
	}
}
