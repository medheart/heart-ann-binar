package com.licenta.model;

public class HeartDisease {
   
 


	float predicted; // min 0 and 1 max

	float age; // 3 - 120 int number
	public static final float ageMin = 18.0f;
	public static final float ageMax = 77.0f;
	public static final float ageFrequent = 60.0f;
	
	float gender; // (M=1)(F=0) min 0 max 1
	public static final float genderMin = 0.0f;
	public static final float genderMax = 1.0f;
	public static final float genderFrequent = 1.0f;
	
	

	float chestPainType; // --Value 1:typical angina
	// --Value 2: atypical anginal
	// --Value 3: non-anginal pain
	// --Value 4: asymptotic
	// min 1 max 4 . 4 type
	public static final float chestPainTypeMin = 1.0f;
	public static final float chestPainTypeMax = 4.0f;
	public static final float chestPainTypeFrequent = 4.0f;

	float restingBloodPressure; // min 94 and max 200 real
	public static final float restingBloodPressureMin = 90.0f;
	public static final float restingBloodPressureMax = 200.0f;
	public static final float restingBloodPressureFrequent = 130.0f;
	
	
	float cholesterol; // min 126 and max 564 real 
	public static final float cholesterolMin = 120.0f;
	public static final float cholesterolMax = 570.0f;
	public static final float cholesterolFrequent = 210.0f;
	
	float fastingBloodSugar; 
	// (fasting blood sugar > 120 mg/dl) (1 = true; 0 = false)
	public static final float fastingBloodSugarMin = 0f;
	public static final float fastingBloodSugarMax = 1f;
	public static final float fastingBloodSugarFrequent = 0f;
	
	float restingECG; // --Value 0: normal
	// --Value 1:having ST-T wave abnormality (T wave inversions and/or ST)
	// --Value 2:showing probable or definite left ventricular Hypertrophy by
	// Estes’ criteria
	// EGG min 0 max 2
	public static final float restingECGMin = 0f;
	public static final float restingECGMax = 2f;
	public static final float restingECGFrequent = 0f;
	
	float maximumHeartRate; // 40 - 200 bpm min 71 and max 202
	public static final float maximumHeartRateMin = 70.0f;
	public static final float maximumHeartRateMax = 200.0f;
	public static final float maximumHeartRateFrequent = 155.0f;

	float exerciseInducedAngina; // (1=yes;0=no)
	public static final float exerciseInducedAnginaMin = 0.0f;
	public static final float exerciseInducedAnginaMax = 1.0f;
	public static final float exerciseInducedAnginaFrequent = 0.0f;
	
	float oldPeak;
	public static final float oldPeakMin = 0.0f;
	public static final float oldPeakMax = 6.2f;
	public static final float oldPeakFrequent = 0.3f;
	
	float slop;// --Value 1: up sloping
	// --Value 2: flat
	// --Value 3:down sloping
	// min 1 max 3
	public static final float slopMin = 1.0f;
	public static final float slopMax = 3.0f;
	public static final float slopFrequent = 1f;
	
	float numberOfVesselsColored; // --(0-3) min 0 , max 3
	public static final float numberOfVesselsColoredMin = 0.0f;
	public static final float numberOfVesselsColoredMax = 3.0f;
	public static final float numberOfVesselsColoredFrequent = 0.0f;
	
	float thal;// Normal, fixed defect, reversible defect --3,6,7
	public static final float thalMin = 3.0f;
	public static final float thalMax = 7.0f;
	public static final float thalFrequent = 3.0f;
	
	@Override
	public String toString() {
		return "DiseaseModel{" + "age=" + age + ", chestPainType=" + chestPainType + ", restingBloodPresure="
				+ restingBloodPressure + ", cholesterol=" + cholesterol + ", restingBloodSugar=" + fastingBloodSugar
				+ ", restingElectroCardiographic=" + restingECG + ", maximumHeartRate="
				+ maximumHeartRate + ", predicted=" + predicted + '}';
	}
}
