package com.licenta.model;

public enum ChestPainType {
	typical_angina(1.0), /* o durere in spatele sternului, o gherea in piept, iradiaza oe umarul drept ( normala) 
	dar poate fi si altfel. Dureaza 5-10 min durerea */
	atypical_angina(2.0),
	non_anginal_pain(3.0),
	asymptomatic(4.0);
	
	private Double value;
	
	private ChestPainType(Double value) {
		this.setValue(value);
	}

	private void setValue(Double value) {
		this.value = value;
		
	}

	public Double getValue() {
		return value;
	}

}
